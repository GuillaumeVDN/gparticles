package com.guillaumevdn.gparticles;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.GPlugin;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.concurrency.RWHashMap;
import com.guillaumevdn.gcore.lib.location.Point;
import com.guillaumevdn.gcore.lib.string.TextFile;
import com.guillaumevdn.gcore.libs.com.google.gson.GsonBuilder;
import com.guillaumevdn.gparticles.command.CmdGadget;
import com.guillaumevdn.gparticles.command.CmdMain;
import com.guillaumevdn.gparticles.command.CmdParticle;
import com.guillaumevdn.gparticles.command.CmdTrail;
import com.guillaumevdn.gparticles.data.user.BoardUsersGP;
import com.guillaumevdn.gparticles.data.user.UserGP;
import com.guillaumevdn.gparticles.lib.gadget.active.ActiveGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetTypes;
import com.guillaumevdn.gparticles.lib.gui.item.type.GUIItemTypesGP;
import com.guillaumevdn.gparticles.lib.particle.displayer.types.DisplayerTypes;
import com.guillaumevdn.gparticles.lib.serialization.SerializerGP;
import com.guillaumevdn.gparticles.lib.serialization.adapter.AdapterUserGP;
import com.guillaumevdn.gparticles.lib.trail.active.TrailBlock;
import com.guillaumevdn.gparticles.listeners.ConnectionEvents;
import com.guillaumevdn.gparticles.listeners.PlayerEvents;
import com.guillaumevdn.gparticles.task.ParticlesAndTrailsRunner;

/**
 * @author GuillaumeVDN
 */
public final class GParticles extends GPlugin<ConfigGP, PermissionGP> {

	private static GParticles instance;
	public static GParticles inst() { return instance; }

	public GParticles() {
		super(10225, "gparticles", "gp", ConfigGP.class, PermissionGP.class, "data_v6");
		instance = this;
	}

	@Override
	public GsonBuilder createGsonBuilder() {
		return super.createGsonBuilder().registerTypeAdapter(UserGP.class, AdapterUserGP.INSTANCE.getGsonAdapter());
	}

	// ----- plugin

	private DisplayerTypes displayerTypes;
	private GadgetTypes gadgetTypes;
	private RWHashMap<Point, TrailBlock> trailBlocks = new RWHashMap<>(5, 1f);
	private RWHashMap<Player, ActiveGadget> activeGadgets = new RWHashMap<>(5, 1f);

	public DisplayerTypes getDisplayerTypes() {
		return displayerTypes;
	}

	public GadgetTypes getGadgetTypes() {
		return gadgetTypes;
	}

	public RWHashMap<Point, TrailBlock> getTrailBlocks() {
		return trailBlocks;
	}

	public RWHashMap<Player, ActiveGadget> getActiveGadgets() {
		return activeGadgets;
	}

	@Override
	protected void registerTypes() {
		displayerTypes = new DisplayerTypes();
		gadgetTypes = new GadgetTypes();
		SerializerGP.init();
		GUIItemTypesGP.init();
	}

	@Override
	protected void registerTexts() {
		registerTextFile(new TextFile<>(GParticles.inst(), "gparticles.yml", TextGP.class));
		registerTextFile(new TextFile<>(GParticles.inst(), "gparticles_editor.yml", TextEditorGP.class));
	}

	@Override
	protected void registerData() {
		registerDataBoard(new BoardUsersGP());
	}

	@Override
	protected Subcommand buildMainCommandBase() {
		return new CmdMain();
	}

	@Override
	protected void enable() throws Throwable {
		registerListener("connection", new ConnectionEvents());
		registerListener("player", new PlayerEvents());
		registerTask("particles_and_trails_runner", true, 1, new ParticlesAndTrailsRunner());

		getMainCommand().setSubcommand(new CmdParticle());
		getMainCommand().setSubcommand(new CmdTrail());
		getMainCommand().setSubcommand(new CmdGadget());
	}

	@Override
	protected void disable() throws Throwable {
		trailBlocks.forEach((point, block) -> {
			block.restore();
		});
		trailBlocks.clear();
		activeGadgets.forEach((player, gadget) -> {
			gadget.stop(false);
		});
		activeGadgets.clear();
	}

}
