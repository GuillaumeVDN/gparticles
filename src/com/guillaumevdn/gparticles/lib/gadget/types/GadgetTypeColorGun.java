package com.guillaumevdn.gparticles.lib.gadget.types;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.compatibility.sound.Sound;
import com.guillaumevdn.gcore.lib.concurrency.RWHashMap;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.element.type.container.ElementItemMode;
import com.guillaumevdn.gcore.lib.location.LocationUtils;
import com.guillaumevdn.gcore.lib.location.Point;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.gadget.active.ActiveGadget;
import com.guillaumevdn.gparticles.lib.gadget.active.GadgetUtils;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;
import com.guillaumevdn.gparticles.lib.trail.active.TrailBlock;

/**
 * @author GuillaumeVDN
 */
public final class GadgetTypeColorGun extends GadgetType {

	private static final List<Mat> WOOL_TYPES = Stream
			.of("BLACK_WOOL", "BLUE_WOOL", "BROWN_WOOL", "CYAN_WOOL", "GRAY_WOOL", "GREEN_WOOL", "LIGHT_BLUE_WOOL", "LIGHT_GRAY_WOOL", "LIME_WOOL", "MAGENTA_WOOL", "ORANGE_WOOL", "PINK_WOOL", "PURPLE_WOOL", "RED_WOOL", "WHITE_WOOL", "YELLOW_WOOL")
			.map(name -> Mat.firstFromIdOrDataName(name).orAir())
			.filter(mat -> !mat.isAir())
			.collect(Collectors.toList());

	private static final List<Mat> CARPET_TYPES = Stream
			.of("BLACK_CARPET", "BLUE_CARPET", "BROWN_CARPET", "CYAN_CARPET", "GRAY_CARPET", "GREEN_CARPET", "LIGHT_BLUE_CARPET", "LIGHT_GRAY_CARPET", "LIME_CARPET", "MAGENTA_CARPET", "ORANGE_CARPET", "PINK_CARPET", "PURPLE_CARPET", "RED_CARPET", "WHITE_CARPET", "YELLOW_CARPET")
			.map(name -> Mat.firstFromIdOrDataName(name).orAir())
			.filter(mat -> !mat.isAir())
			.collect(Collectors.toList());

	public GadgetTypeColorGun(String id) {
		super(id, CommonMats.BEDROCK);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGadget gadget) {
		super.doFillTypeSpecificElements(gadget);
		gadget.addDuration("duration", Need.required(), null, null, null);
		gadget.addDuration("persistence_time", Need.required(), null, null, null);
		gadget.addInteger("radius", Need.required(), null);
		gadget.addItem("gun_item", Need.required(), ElementItemMode.BUILDABLE, null);
		gadget.addSound("sound_shoot", Need.optional(), null);
	}

	@Override
	public ActiveGadget create(ElementGadget gadget, Player player) throws ParsingError {
		final Replacer rep = Replacer.of(player);

		final int duration = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("duration", rep) / 50L);
		final long blockPersistenceMillis = gadget.directParseNoCatchOrThrowParsingNull("persistence_time", rep);
		final int radius = gadget.directParseNoCatchOrThrowParsingNull("radius", rep);
		final ItemStack gunItem = gadget.directParseNoCatchOrThrowParsingNull("gun_item", rep);
		final Sound soundShoot = gadget.directParseOrNull("sound_shoot", rep);

		final int slot = ConfigGP.hotbarSlotColorGun < 0 || ConfigGP.hotbarSlotColorGun > 8 ? 5 : ConfigGP.hotbarSlotColorGun;

		return new ActiveGadget(gadget, player) {

			private final String id = "gadget_" + player.getName() + "_" + StringUtils.generateRandomAlphanumericString(10);

			private ItemStack ogItem = null;
			private int remainingTicks = duration;
			private List<Snowball> snowballs = new ArrayList<>();
			private RWHashMap<Point, TrailBlock> trailBlocks = new RWHashMap<>(5, 1f);

			@Override
			protected void doStart() {
				ogItem = player.getInventory().getItem(slot);
				player.getInventory().setItem(slot, gunItem);
				player.updateInventory();

				GParticles.inst().registerTask(id, false, 1, () -> tick());
				GParticles.inst().registerListener(id, this);
			}

			private void tick() {
				// done
				if (--remainingTicks <= 0) {
					stop();
					return;
				}

				// remove old trail blocks
				trailBlocks.iterateAndModify((p, tb, it) -> {
					if (System.currentTimeMillis() - tb.getChangedAt() >= blockPersistenceMillis) {
						tb.restore();
						it.remove();
						GParticles.inst().getTrailBlocks().remove(p);
					}
				});
			}

			@EventHandler(priority = EventPriority.LOWEST)
			public void event(PlayerInteractEvent event) {
				if (!event.getAction().toString().contains("RIGHT_CLICK")) {
					return;
				}

				final Player player = event.getPlayer();
				if (player.getInventory().getHeldItemSlot() != slot) {
					return;
				}

				event.setCancelled(true);

				final Snowball snowball = player.launchProjectile(Snowball.class, null);
				snowballs.add(snowball);

				if (soundShoot != null) {
					soundShoot.play(player.getWorld().getPlayers(), player.getLocation().add(0d, 1d, 0d));
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(ProjectileHitEvent event) {
				final Projectile proj = event.getEntity();
				if (!snowballs.remove(proj)) {
					return;
				}

				final Location loc = event.getHitBlock() != null ? event.getHitBlock().getLocation() : proj.getLocation();
				final List<Block> blocks = LocationUtils.getSphereBlocks(loc, radius);
				final Mat targetMatCarpet = CollectionUtils.random(CARPET_TYPES);
				final Mat targetMatWool = CollectionUtils.random(WOOL_TYPES);

				for (Block block : blocks) {
					final Mat blockType = Mat.fromBlock(block).orAir();
					if (!GadgetUtils.mustIgnoreBlock(blockType)) {
						// only replace blocks that aren't already colored
						final Point point = new Point(block);
						if (!GParticles.inst().getTrailBlocks().containsKey(point)) {
							// set trail block
							final TrailBlock tblock = new TrailBlock(point);
							GParticles.inst().getTrailBlocks().put(point, tblock);
							trailBlocks.put(point, tblock);

							// send block change
							final Mat targetMat = blockType.getData().getDataName().contains("CARPET") ? targetMatCarpet : targetMatWool;
							targetMat.setBlockChange(block);
						}
					}
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityDamageByEntityEvent event) {
				if (snowballs.contains(event.getDamager())) {
					event.setDamage(0d);
					event.setCancelled(true);
				}
			}

			@Override
			protected void doStop() {
				GParticles.inst().stopTask(id);
				GParticles.inst().stopListener(id);

				snowballs.forEach(ball -> ball.remove());

				trailBlocks.forEach((point, tblock) -> {
					tblock.restore();
					GParticles.inst().getTrailBlocks().remove(point);
				});

				player.getInventory().setItem(slot, ogItem);
				player.updateInventory();
			}

		};
	}

}
