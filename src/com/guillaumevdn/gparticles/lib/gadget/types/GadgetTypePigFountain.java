package com.guillaumevdn.gparticles.lib.gadget.types;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.compatibility.sound.Sound;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.gadget.active.ActiveGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;

/**
 * @author GuillaumeVDN
 */
public final class GadgetTypePigFountain extends GadgetType {

	private static final List<ItemStack> DROP_ITEMS = Stream
			.of("BONE_MEAL", "RED_DYE")
			.map(name -> Mat.firstFromIdOrDataName(name).orAir())
			.filter(mat -> !mat.isAir())
			.map(mat -> mat.newStack())
			.collect(Collectors.toList());

	public GadgetTypePigFountain(String id) {
		super(id, CommonMats.BEDROCK);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGadget gadget) {
		super.doFillTypeSpecificElements(gadget);
		gadget.addDuration("duration", Need.required(), null, null, null);
		gadget.addDuration("spawn_delay", Need.required(), null, null, null);
		gadget.addInteger("item_count", Need.required(), 1, null, null);
		gadget.addSound("sound_spawn", Need.optional(), null);
		gadget.addSound("sound_fall", Need.optional(), null);
	}

	@Override
	public ActiveGadget create(ElementGadget gadget, Player player) throws ParsingError {
		final Replacer rep = Replacer.of(player);

		final int durationTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("duration", rep) / 50L);
		final int spawnDelayTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("spawn_delay", rep) / 50L);
		final int itemCount = gadget.directParseNoCatchOrThrowParsingNull("item_count", rep);
		final Sound soundSpawn = gadget.directParseOrNull("sound_spawn", rep);
		final Sound soundFall = gadget.directParseOrNull("sound_fall", rep);

		return new ActiveGadget(gadget, player) {

			private final String id = "gadget_" + player.getName() + "_" + StringUtils.generateRandomAlphanumericString(10);
			private final Location base = player.getLocation();

			private int remainingTicks = durationTicks;
			private int ticksBeforeSpawn = 0;
			private List<Pig> pigs = new ArrayList<>();
			private List<Item> items = new ArrayList<>();

			@Override
			protected void doStart() {
				GParticles.inst().registerTask(id, false, 1, () -> tick());
				GParticles.inst().registerListener(id, this);
			}

			private void tick() {
				// stop
				if (--remainingTicks <= 0) {
					stop();
					return;
				}

				// spawn
				if (--ticksBeforeSpawn <= 0) {
					ticksBeforeSpawn = spawnDelayTicks;

					// spawn pig
					Pig pig = (Pig) base.getWorld().spawnEntity(base, EntityType.PIG);
					pig.setVelocity(new Vector(NumberUtils.random(-0.5d, 0.5d), NumberUtils.random(0.5d, 2d), NumberUtils.random(-0.5d, 0.5d)));
					pigs.add(pig);

					// sound
					if (soundSpawn != null) {
						soundSpawn.play(pig.getLocation());
					}
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityDamageEvent event) {
				if (pigs.contains(event.getEntity())) {
					event.setDamage(0d);
					event.setCancelled(true);

					if (event.getCause().equals(DamageCause.FALL)) {
						Pig pig = (Pig) event.getEntity();

						// sound
						if (soundFall != null) {
							soundFall.play(pig.getLocation());
						}

						// spawn items
						for (int i = 0; i < itemCount; i++) {
							Item item = pig.getWorld().dropItem(pig.getLocation().clone().add(0d, 1d, 0d), CollectionUtils.random(DROP_ITEMS));
							item.setPickupDelay(Integer.MAX_VALUE);
							item.setVelocity(new Vector(NumberUtils.random(-0.2d, 0.2d), NumberUtils.random(0d, 0.2d), NumberUtils.random(-0.2d, 0.2d)));
							items.add(item);
						}

						// remove entity
						pig.remove();
						pigs.remove(pig);
					}
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(EntityCombustEvent event) {
				if (pigs.contains(event.getEntity())) {
					event.setCancelled(true);
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void onHopperHop(InventoryPickupItemEvent event) {
				if (items.contains(event.getItem())) {
					event.setCancelled(true);
				}
			}

			@Override
			protected void doStop() {
				GParticles.inst().stopTask(id);
				GParticles.inst().stopListener(id);

				pigs.forEach(pig -> pig.remove());
				items.forEach(it -> it.remove());
			}

		};
	}

}
