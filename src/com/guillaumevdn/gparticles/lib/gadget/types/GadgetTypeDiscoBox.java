package com.guillaumevdn.gparticles.lib.gadget.types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.compatibility.particle.Particle;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.gadget.active.ActiveGadget;
import com.guillaumevdn.gparticles.lib.gadget.active.GadgetUtils;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;

/**
 * @author GuillaumeVDN
 */
public final class GadgetTypeDiscoBox extends GadgetType {

	private static final List<Mat> GLASS_TYPES = Stream
			.of("BLACK_STAINED_GLASS", "BLUE_STAINED_GLASS", "BROWN_STAINED_GLASS", "CYAN_STAINED_GLASS", "GRAY_STAINED_GLASS", "GREEN_STAINED_GLASS", "LIGHT_BLUE_STAINED_GLASS", "LIGHT_GRAY_STAINED_GLASS", "LIME_STAINED_GLASS", "MAGENTA_STAINED_GLASS", "ORANGE_STAINED_GLASS", "PINK_STAINED_GLASS", "PURPLE_STAINED_GLASS", "RED_STAINED_GLASS", "WHITE_STAINED_GLASS", "YELLOW_STAINED_GLASS")
			.map(name -> Mat.firstFromIdOrDataName(name).orAir())
			.filter(mat -> !mat.isAir())
			.collect(Collectors.toList());

	private static final List<Mat> GLASS_PANE_TYPES = Stream
			.of("BLACK_STAINED_GLASS_PANE", "BLUE_STAINED_GLASS_PANE", "BROWN_STAINED_GLASS_PANE", "CYAN_STAINED_GLASS_PANE", "GRAY_STAINED_GLASS_PANE", "GREEN_STAINED_GLASS_PANE", "LIGHT_BLUE_STAINED_GLASS_PANE", "LIGHT_GRAY_STAINED_GLASS_PANE", "LIME_STAINED_GLASS_PANE", "MAGENTA_STAINED_GLASS_PANE", "ORANGE_STAINED_GLASS_PANE", "PINK_STAINED_GLASS_PANE", "PURPLE_STAINED_GLASS_PANE", "RED_STAINED_GLASS_PANE", "WHITE_STAINED_GLASS_PANE", "YELLOW_STAINED_GLASS_PANE")
			.map(name -> Mat.firstFromIdOrDataName(name).orAir())
			.filter(mat -> !mat.isAir())
			.collect(Collectors.toList());

	private static final Particle PARTICLE_REDSTONE = Particle.firstFromIdOrDataName("REDSTONE").orNull();

	public GadgetTypeDiscoBox(String id) {
		super(id, CommonMats.BEDROCK);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGadget gadget) {
		super.doFillTypeSpecificElements(gadget);
		gadget.addDuration("duration", Need.required(), null, null, null);
		gadget.addDuration("switch_delay", Need.required(), null, null, null);
		gadget.addInteger("radius", Need.optional(), null);
		gadget.addInteger("particle_count", Need.optional(), null);
		gadget.addMatList("discs", Need.optional(), null);
	}

	@Override
	public ActiveGadget create(ElementGadget gadget, Player player) throws ParsingError {
		final Replacer rep = Replacer.of(player);

		final int durationTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("duration", rep) / 50L);
		final int switchDelayTicks = (int) ((long) gadget.directParseNoCatchOrThrowParsingNull("switch_delay", rep) / 50L);
		final int radius = gadget.directParseNoCatchOrThrowParsingNull("radius", rep);
		final int particleCount = gadget.directParseNoCatchOrThrowParsingNull("particle_count", rep);
		final List<Mat> discs = gadget.parseElementAsList("discs", Mat.class, rep).orEmptyList();

		return new ActiveGadget(gadget, player) {

			private final String id = "gadget_" + player.getName() + "_" + StringUtils.generateRandomAlphanumericString(10);

			private int remainingTicks = durationTicks;
			private int ticksBeforeSwitch = 0;
			private Location base = player.getLocation().clone().subtract(0, 1, 0);
			private Block jukeboxBlock = base.getBlock();
			private Block lightBlock = base.clone().add(0, 4, 0).getBlock();
			private Mat jukeboxBlockMatOg = Mat.fromBlock(jukeboxBlock).orAir();
			private Mat lightBlockMatOg = Mat.fromBlock(lightBlock).orAir();
			private Map<Block, Mat> borders = new HashMap<>();
			private Map<Block, Mat> floorAndCeiling = new HashMap<>();
			private boolean lightToggled = true;

			@Override
			protected void doStart() {
				// pre-calculate blocks
				for (int x : CollectionUtils.asList(radius, -radius)) {
					for (int z = -radius; z <= radius; z++) {
						for (int y = 1; y <= 3; y++) {
							final Block block = base.clone().add(x, y, z).getBlock();
							if (!GadgetUtils.mustIgnoreBlockDiscoBox(block)) {
								borders.put(block, Mat.fromBlock(block).orAir());
							}
						}
					}
				}
				for (int z : CollectionUtils.asList(radius, -radius)) {
					for (int x = -radius; x <= radius; x++) {
						for (int y = 1; y <= 3; y++) {
							final Block block = base.clone().add(x, y, z).getBlock();
							if (!GadgetUtils.mustIgnoreBlockDiscoBox(block)) {
								borders.put(block, Mat.fromBlock(block).orAir());
							}
						}
					}
				}
				for (int y : CollectionUtils.asList(0, 4)) {
					for (int x = -radius; x <= radius; x++) {
						for (int z = -radius; z <= radius; z++) {
							if (x == 0 && z == 0) continue;  // don't add center top and bottom (jukebox or light)
							final Block block = base.clone().add(x, y, z).getBlock();
							if (!GadgetUtils.mustIgnoreBlockDiscoBox(block)) {
								floorAndCeiling.put(block, Mat.fromBlock(block).orAir());
							}
						}
					}
				}

				// set blocks
				CommonMats.GLOWSTONE.setBlockChange(lightBlock);
				CommonMats.JUKEBOX.setBlockChange(jukeboxBlock);

				// play disc
				try {
					final Mat disc = CollectionUtils.random(discs);
					if (disc != null) {
						base.getWorld().playEffect(lightBlock.getLocation(), Effect.RECORD_PLAY, disc.getData().getDataInstance());
					}
				} catch (Throwable exception) {
					exception.printStackTrace();
					GParticles.inst().getMainLogger().error("Couldn't play disc for the Disco Box gadget");
				}

				// start task and listener
				GParticles.inst().registerTask(id, false, 1, () -> tick());
				GParticles.inst().registerListener(id, this);
			}

			private void tick() {
				// stop
				if (--remainingTicks <= 0) {
					stop();
					return;
				}

				// switch
				if (--ticksBeforeSwitch <= 0) {
					ticksBeforeSwitch = switchDelayTicks;

					// borders
					for (Block block : borders.keySet()) {
						CollectionUtils.random(GLASS_PANE_TYPES).setBlockChange(block);
					}
					for (Block block : floorAndCeiling.keySet()) {
						CollectionUtils.random(GLASS_TYPES).setBlockChange(block);
					}

					// light
					lightToggled = !lightToggled;
					(lightToggled ? CommonMats.GLOWSTONE : CollectionUtils.random(GLASS_TYPES)).setBlockChange(lightBlock);
				}

				// particles
				if (PARTICLE_REDSTONE != null) {
					final List<Player> players = base.getWorld().getPlayers();
					final double r = (double) radius;
					for (int i = 0; i < particleCount; i++) {
						final Location loc = base.clone().add(NumberUtils.random(-r, r), NumberUtils.random(0d, 5d), NumberUtils.random(-r, r));
						final Color color = Color.fromRGB(NumberUtils.random(0, 255), NumberUtils.random(0, 255), NumberUtils.random(0, 255));
						for (Player pl : players) {
							PARTICLE_REDSTONE.send(pl, loc, color, null, 1, 1f);
						}
					}
				}
			}

			@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
			public void event(BlockBreakEvent event) {
				final Block block = event.getBlock();
				if (borders.containsKey(block) || floorAndCeiling.containsKey(block) || block.equals(lightBlock) || block.equals(jukeboxBlock)) {
					event.setCancelled(true);
				}
			}

			@Override
			protected void doStop() {
				GParticles.inst().stopTask(id);
				GParticles.inst().stopListener(id);

				base.getWorld().playEffect(lightBlock.getLocation(), Effect.RECORD_PLAY, 0);

				borders.forEach((block, type) -> type.setBlockChange(block));
				floorAndCeiling.forEach((block, type) -> type.setBlockChange(block));
				jukeboxBlockMatOg.setBlockChange(jukeboxBlock);
				lightBlockMatOg.setBlockChange(lightBlock);
			}

		};
	}

}
