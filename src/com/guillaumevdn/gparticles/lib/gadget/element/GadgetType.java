package com.guillaumevdn.gparticles.lib.gadget.element;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.TextGeneric;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.TypableElementType;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.TextGP;
import com.guillaumevdn.gparticles.data.user.UserGP;
import com.guillaumevdn.gparticles.lib.gadget.active.ActiveGadget;

/**
 * @author GuillaumeVDN
 */
public abstract class GadgetType extends TypableElementType<ElementGadget> {

	public GadgetType(String id, Mat icon) {
		super(id, icon);
	}

	public abstract ActiveGadget create(ElementGadget gadget, Player player) throws ParsingError;

	public final boolean attemptStart(ElementGadget gadget, Player player) {
		final Replacer rep = Replacer.of(player);

		// already has an active gadget
		if (GParticles.inst().getActiveGadgets().containsKey(player)) {
			return false;
		}

		// permission
		final Permission permission = gadget.getPermission().parse(rep).orNull();
		if (permission != null && !permission.has(player)) {
			TextGeneric.messageNoPermission.send(player);
			return false;
		}

		// cooldown
		final UserGP user = UserGP.cachedOrNull(player);
		if (user == null) {
			return false;
		}
		final Long last = user.getLastGadgetUse(gadget.getType());
		if (last != null) {
			final Long cooldown = gadget.getCooldown().parse(rep).orNull();
			if (cooldown != null) {
				final long elapsed = System.currentTimeMillis() - last;
				if (elapsed < cooldown) {
					TextGP.messageGadgetCooldown.replace("{time}", () -> StringUtils.formatDurationMillis(cooldown - elapsed)).send(player);
					return false;
				}
			}
		}

		// create active gadget
		final ActiveGadget active;
		try {
			active = gadget.getType().create(gadget, player);
		} catch (ParsingError error) {
			GParticles.inst().getMainLogger().error("Couldn't create gadget " + gadget.getType(), error);
			return false;
		}

		// start
		active.start();
		return true;
	}

}
