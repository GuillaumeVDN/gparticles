package com.guillaumevdn.gparticles.lib.gadget.element;

import com.guillaumevdn.gcore.lib.element.struct.Element;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.ElementTypableElementType;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.TypableContainerElement;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementPermission;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementString;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gcore.lib.time.duration.ElementDuration;

/**
 * @author GuillaumeVDN
 */
public class ElementGadget extends TypableContainerElement<GadgetType> {

	private ElementString name = addString("name", Need.required(), null);
	private ElementPermission permission = addPermission("permission", Need.optional(), null);
	private ElementDuration cooldown = addDuration("cooldown", Need.required(), null, null, null);

	public ElementGadget(Element parent, String id, Need need, Text editorDescription) {
		super(GadgetTypes.inst(), parent, id, need, editorDescription);
	}

	@Override
	protected final ElementTypableElementType<GadgetType> addType() {
		return add(new ElementGadgetType(this, "type", null));
	}

	public ElementString getName() {
		return name;
	}

	public ElementPermission getPermission() {
		return permission;
	}

	public ElementDuration getCooldown() {
		return cooldown;
	}

}
