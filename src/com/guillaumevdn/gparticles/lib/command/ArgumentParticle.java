package com.guillaumevdn.gparticles.lib.command;

import java.util.List;
import java.util.stream.Collectors;

import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.argument.Argument;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.lib.particle.ElementParticleGP;

/**
 * @author GuillaumeVDN
 */
public class ArgumentParticle extends Argument<ElementParticleGP> {

	public ArgumentParticle(NeedType need, boolean playerOnly, Permission permission, Text usage) {
		super(need, playerOnly, permission, usage);
	}

	@Override
	public ElementParticleGP consume(CommandCall call) {
		if (call.getArguments().isEmpty())
			return null;
		for (int i = 0; i < call.getArguments().size(); ++i) {
			final ElementParticleGP particle = ConfigGP.particles.getElement(call.getArguments().get(i).toLowerCase()).orNull();
			if (particle != null) {
				call.getArguments().remove(i);
				return particle;
			}
		}
		return null;
	}

	@Override
	public List<String> tabComplete(CommandCall call) {
		return ConfigGP.particles.values().stream().map(ElementParticleGP::getId).sorted(String::compareTo).collect(Collectors.toList());
	}

}
