package com.guillaumevdn.gparticles.lib.gui;

import java.io.File;

import com.guillaumevdn.gcore.lib.GPlugin;
import com.guillaumevdn.gcore.lib.gui.element.ElementGUI;
import com.guillaumevdn.gparticles.GParticles;

/**
 * @author GuillaumeVDN
 */
public class ElementGUIGP extends ElementGUI {

	public ElementGUIGP(File file, String id) {
		super(file, id, true);
	}

	@Override
	public GPlugin getPlugin() {
		return GParticles.inst();
	}

}
