package com.guillaumevdn.gparticles.lib.gui.item.type;

import java.util.Set;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.function.QuadriConsumer;
import com.guillaumevdn.gcore.lib.gui.element.item.ElementGUIItem;
import com.guillaumevdn.gcore.lib.gui.element.item.type.GUIItemType;
import com.guillaumevdn.gcore.lib.gui.element.item.type.IconNeed;
import com.guillaumevdn.gcore.lib.gui.struct.ClickCall;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveGUI;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolder;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolderElementGUIItemCommon;
import com.guillaumevdn.gcore.lib.gui.struct.active.ItemHolder;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gparticles.TextGP;
import com.guillaumevdn.gparticles.data.user.UserGP;

/**
 * @author GuillaumeVDN
 */
public class TypeParticleRemove extends GUIItemType {

	public TypeParticleRemove(String id) {
		super(id, IconNeed.REQUIRED, CommonMats.EMERALD);
	}

	@Override
	public ActiveItemHolder newActive(ActiveGUI instance, ItemHolder holder, ElementGUIItem element) {
		return new ActiveItemHolderElementGUIItemCommon(instance, holder, element) {
			@Override
			protected void build(ItemStack itemIcon, QuadriConsumer<ItemStack, Set<String>, Integer, Consumer<ClickCall>> callback) throws ParsingError {

				final Player player = instance.getReplacer().getReplacerData().getPlayer();
				final UserGP user = UserGP.cachedOrNull(player);
				if (user == null || user.getActiveParticleId() == null) {
					instance.getPlugin().operateAsync(() -> {
						instance.directRemove(holder);  // might be already built
					});
					return;
				}

				Set<String> placeholders = StringUtils.getPlaceholders(itemIcon);
				itemIcon = getInstance().getReplacer().parse(itemIcon);

				callback.accept(itemIcon, placeholders, -1, call -> {
					if (user.getActiveParticleId() != null) {
						user.setActiveParticle(null);
						TextGP.messageParticleDisable.send(call);
						instance.refreshOfType(GUIItemTypesGP.GP_PARTICLE);
						instance.refreshOfType(GUIItemTypesGP.GP_PARTICLE_REMOVE);
					}
				});
			}
		};
	}

}
