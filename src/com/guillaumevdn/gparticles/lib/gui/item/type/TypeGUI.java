package com.guillaumevdn.gparticles.lib.gui.item.type;

import java.util.Set;
import java.util.function.Consumer;

import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.parsing.ParsingError;
import com.guillaumevdn.gcore.lib.function.QuadriConsumer;
import com.guillaumevdn.gcore.lib.gui.element.ElementGUI;
import com.guillaumevdn.gcore.lib.gui.element.item.ElementGUIItem;
import com.guillaumevdn.gcore.lib.gui.element.item.type.GUIItemType;
import com.guillaumevdn.gcore.lib.gui.element.item.type.IconNeed;
import com.guillaumevdn.gcore.lib.gui.struct.ClickCall;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveGUI;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolder;
import com.guillaumevdn.gcore.lib.gui.struct.active.ActiveItemHolderElementGUIItemCommon;
import com.guillaumevdn.gcore.lib.gui.struct.active.ItemHolder;
import com.guillaumevdn.gparticles.TextEditorGP;
import com.guillaumevdn.gparticles.lib.gui.ElementGUIGPReference;

/**
 * @author GuillaumeVDN
 */
public class TypeGUI extends GUIItemType {

	public TypeGUI(String id) {
		super(id, IconNeed.REQUIRED, CommonMats.ENDER_CHEST);
	}

	@Override
	protected void doFillTypeSpecificElements(ElementGUIItem item) {
		super.doFillTypeSpecificElements(item);
		item.add(new ElementGUIGPReference(item, "gui", Need.required(), TextEditorGP.elementGUIItemTypeGUIGUI));
	}

	@Override
	public ActiveItemHolder newActive(ActiveGUI instance, ItemHolder holder, ElementGUIItem element) {
		return new ActiveItemHolderElementGUIItemCommon(instance, holder, element) {
			@Override
			protected void build(ItemStack itemIcon, QuadriConsumer<ItemStack, Set<String>, Integer, Consumer<ClickCall>> callback) throws ParsingError {

				final ElementGUI targetGUI = element.directParseNoCatchOrThrowParsingNull("gui", instance.getReplacer());

				itemIcon = getInstance().getReplacer().parse(itemIcon);

				callback.accept(itemIcon, null, -1, call -> {
					targetGUI.build(instance.getReplacer()).openFor(call.getClicker(), call.getAncestorFor(targetGUI));
				});
			}
		};
	}

}
