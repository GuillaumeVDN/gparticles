package com.guillaumevdn.gparticles.lib.serialization.adapter;

import java.util.UUID;

import com.guillaumevdn.gcore.lib.concurrency.RWLowerCaseHashMap;
import com.guillaumevdn.gcore.lib.serialization.adapter.DataAdapter;
import com.guillaumevdn.gcore.lib.serialization.data.DataIO;
import com.guillaumevdn.gparticles.data.user.UserGP;

/**
 * @author GuillaumeVDN
 */
public final class AdapterUserGP extends DataAdapter<UserGP> {

	public static final AdapterUserGP INSTANCE = new AdapterUserGP();

	private AdapterUserGP() {
		super(UserGP.class, 1);
	}

	@Override
	public void write(UserGP user, DataIO writer) throws Throwable {
		writer.write("playerUUID", user.getPlayerUUID());
		writer.write("activeParticleId", user.getActiveParticleId());
		writer.write("activeTrailId", user.getActiveTrailId());
		writer.writeObject("lastGadgetUses", mapWriter -> {
			user.getLastGadgetUses().forEach((gadgetId, last) -> {
				mapWriter.write(gadgetId, last);
			});
		});
	}

	@Override
	public UserGP read(int version, DataIO reader) throws Throwable {
		if (version == 1) {
			final UUID playerUUID = reader.readUUID("playerUUID");
			final String activeParticleId = reader.readString("activeParticleId");
			final String activeTrailId = reader.readString("activeTrailId");
			final RWLowerCaseHashMap<Long> lastGadgetUses = reader.readSimpleMap("lastGadgetUses", String.class, new RWLowerCaseHashMap<Long>(5, 1f), (key, __, r) -> r.readLong(key));

			return new UserGP(playerUUID, activeParticleId, activeTrailId, lastGadgetUses);
		}
		throw new IllegalArgumentException("unknown adapter version " + version);
	}

}
