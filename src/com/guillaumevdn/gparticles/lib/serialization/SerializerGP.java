package com.guillaumevdn.gparticles.lib.serialization;

import com.guillaumevdn.gcore.lib.serialization.Serializer;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.GParticles;
import com.guillaumevdn.gparticles.lib.gadget.element.ElementGadget;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetTypes;
import com.guillaumevdn.gparticles.lib.gui.ElementGUIGP;
import com.guillaumevdn.gparticles.lib.particle.ElementParticleGP;
import com.guillaumevdn.gparticles.lib.particle.displayer.element.DisplayerType;
import com.guillaumevdn.gparticles.lib.trail.element.ElementTrail;

/**
 * @author GuillaumeVDN
 */
public final class SerializerGP {

	public static final Serializer<ElementGUIGP> GUI = Serializer.ofContainer(ElementGUIGP.class, () -> ConfigGP.guis);
	public static final Serializer<ElementParticleGP> PARTICLE = Serializer.ofContainer(ElementParticleGP.class, () -> ConfigGP.particles);
	public static final Serializer<ElementTrail> TRAIL = Serializer.ofContainer(ElementTrail.class, () -> ConfigGP.trails);
	public static final Serializer<ElementGadget> GADGET = Serializer.of(ElementGadget.class, gadget -> gadget.getType().getId(), typeId -> ConfigGP.gadgets.get(GadgetTypes.inst().safeValueOf(typeId)));

	public static final Serializer<GadgetType> GADGET_TYPE = Serializer.ofTypable(GadgetType.class, () -> GParticles.inst().getGadgetTypes());
	public static final Serializer<DisplayerType> DISPLAYER_TYPE = Serializer.ofTypable(DisplayerType.class, () -> GParticles.inst().getDisplayerTypes());

	public static void init() {}

}
