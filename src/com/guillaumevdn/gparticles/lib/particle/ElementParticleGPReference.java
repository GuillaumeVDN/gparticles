package com.guillaumevdn.gparticles.lib.particle;

import java.util.List;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.element.struct.Element;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementAbstractEnum;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.lib.serialization.SerializerGP;

/**
 * @author GuillaumeVDN
 */
public class ElementParticleGPReference extends ElementAbstractEnum<ElementParticleGP> {

	public ElementParticleGPReference(Element parent, String id, Need need, Text editorDescription) {
		super(SerializerGP.PARTICLE, false, parent, id, need, editorDescription);
	}

	@Override
	public List<ElementParticleGP> getValues() {
		return CollectionUtils.asList(ConfigGP.particles.values());
	}

	@Override
	public Mat editorIconType() {
		return CommonMats.CHEST;
	}

}
