package com.guillaumevdn.gparticles.lib.particle;

import java.io.File;

import com.guillaumevdn.gcore.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcore.lib.element.struct.list.referenceable.ElementsContainer;
import com.guillaumevdn.gparticles.GParticles;

/**
 * @author GuillaumeVDN
 */
public class ParticlesContainer extends ElementsContainer<ElementParticleGP> {

	public ParticlesContainer() {
		super(GParticles.inst(), "particle", ElementParticleGP.class, GParticles.inst().getDataFile("particles/"));
	}

	@Override
	protected ElementParticleGP createElement(File file, String id) {
		return new ElementParticleGP(file, id);
	}

	@Override
	protected void doLoadFile(File file) throws Throwable {
		YMLConfiguration config = new YMLConfiguration(GParticles.inst(), file);
		for (String id : config.readKeysForSection("particles")) {
			super.doLoadFile(file, id);
		}
	}

}
