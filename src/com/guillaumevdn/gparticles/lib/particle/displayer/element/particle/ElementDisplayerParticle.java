package com.guillaumevdn.gparticles.lib.particle.displayer.element.particle;

import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.element.struct.Element;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.container.ContainerElement;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementFloat;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementInteger;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementParticle;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gparticles.TextEditorGP;

/**
 * @author GuillaumeVDN
 */
public class ElementDisplayerParticle extends ContainerElement {

	private ElementParticle particle = addParticle("particle", Need.required(), TextEditorGP.elementParticleDisplayerParticleParticle);
	private ElementInteger colorRed = addInteger("color_red", Need.optional(), 0, 255, TextEditorGP.elementParticleDisplayerParticleColorRed);
	private ElementInteger colorGreen = addInteger("color_green", Need.optional(), 0, 255, TextEditorGP.elementParticleDisplayerParticleColorGreen);
	private ElementInteger colorBlue = addInteger("color_blue", Need.optional(), 0, 255, TextEditorGP.elementParticleDisplayerParticleColorBlue);
	private ElementFloat redstoneColorScale = addFloat("redstone_color_scale", Need.optional(1f), TextEditorGP.elementParticleDisplayerParticleRedstoneColorScale);
	private ElementInteger particleNoteColor = addInteger("particle_note_color", Need.optional(), TextEditorGP.elementParticleDisplayerParticleNoteColor);

	public ElementDisplayerParticle(Element parent, String id, Need need, Text editorDescription) {
		super(parent, id, need, editorDescription);
	}

	public ElementParticle getParticle() {
		return particle;
	}

	public ElementInteger getColorRed() {
		return colorRed;
	}

	public ElementInteger getColorGreen() {
		return colorGreen;
	}

	public ElementInteger getColorBlue() {
		return colorBlue;
	}

	public ElementFloat getRedstoneColorScale() {
		return redstoneColorScale;
	}

	public ElementInteger getParticleNoteColor() {
		return particleNoteColor;
	}

	@Override
	public Mat editorIconType() {
		return CommonMats.EMERALD;
	}

}
