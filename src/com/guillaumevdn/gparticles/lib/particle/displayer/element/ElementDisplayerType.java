package com.guillaumevdn.gparticles.lib.particle.displayer.element;

import java.util.Comparator;
import java.util.List;

import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.concurrency.RWWeakHashMap;
import com.guillaumevdn.gcore.lib.element.struct.Element;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.ElementTypableElementType;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gparticles.lib.particle.displayer.types.DisplayerTypes;

/**
 * @author GuillaumeVDN
 */
public final class ElementDisplayerType extends ElementTypableElementType<DisplayerType> {

	public ElementDisplayerType(Element parent, String id, Text editorDescription) {
		super(DisplayerTypes.inst(), parent, id, editorDescription);
	}

	private static RWWeakHashMap<Object, List<DisplayerType>> cache = new RWWeakHashMap<>(1, 1f);
	@Override
	protected List<DisplayerType> cacheOrBuild() {
		return cachedOrBuild(cache, () -> DisplayerTypes.inst().values().stream().sorted(Comparator.comparing(e -> e.getId())));
	}

	@Override
	public Mat editorIconType() {
		return parseGeneric().ifPresentMap(DisplayerType::getIcon).orElse(CommonMats.REDSTONE);
	}

}
