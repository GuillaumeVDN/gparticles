package com.guillaumevdn.gparticles.lib.particle.displayer.element;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.TypableElementType;
import com.guillaumevdn.gparticles.lib.particle.displayer.active.ParticleRunner;

/**
 * @author GuillaumeVDN
 */
public abstract class DisplayerType extends TypableElementType<ElementDisplayer> {

	public DisplayerType(String id, Mat icon) {
		super(id, icon);
	}

	public abstract ParticleRunner buildRunner(Player player, ElementDisplayer elem);

}
