package com.guillaumevdn.gparticles.lib.trail.element;

import java.util.List;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.element.struct.Element;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementAbstractEnum;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.lib.serialization.SerializerGP;

/**
 * @author GuillaumeVDN
 */
public class ElementTrailReference extends ElementAbstractEnum<ElementTrail> {

	public ElementTrailReference(Element parent, String id, Need need, Text editorDescription) {
		super(SerializerGP.TRAIL, false, parent, id, need, editorDescription);
	}

	@Override
	public List<ElementTrail> getValues() {
		return CollectionUtils.asList(ConfigGP.trails.values());
	}

	@Override
	public Mat editorIconType() {
		return CommonMats.CHEST;
	}

}
