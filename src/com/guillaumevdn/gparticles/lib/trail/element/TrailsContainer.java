package com.guillaumevdn.gparticles.lib.trail.element;

import java.io.File;

import com.guillaumevdn.gcore.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcore.lib.element.struct.list.referenceable.ElementsContainer;
import com.guillaumevdn.gparticles.GParticles;

/**
 * @author GuillaumeVDN
 */
public class TrailsContainer extends ElementsContainer<ElementTrail> {

	public TrailsContainer() {
		super(GParticles.inst(), "trail", ElementTrail.class, GParticles.inst().getDataFile("trails/"));
	}

	@Override
	protected ElementTrail createElement(File file, String id) {
		return new ElementTrail(file, id);
	}

	@Override
	protected void doLoadFile(File file) throws Throwable {
		YMLConfiguration config = new YMLConfiguration(GParticles.inst(), file);
		for (String id : config.readKeysForSection("trails")) {
			super.doLoadFile(file, id);
		}
	}

}
