package com.guillaumevdn.gparticles.data.user;

import java.util.UUID;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.concurrency.RWLowerCaseHashMap;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.lib.gadget.element.GadgetType;
import com.guillaumevdn.gparticles.lib.particle.ElementParticleGP;
import com.guillaumevdn.gparticles.lib.particle.displayer.active.ParticleRunner;
import com.guillaumevdn.gparticles.lib.trail.active.TrailRunner;
import com.guillaumevdn.gparticles.lib.trail.element.ElementTrail;

/**
 * @author GuillaumeVDN
 */
public final class UserGP {

	private final UUID playerUUID;

	private String activeParticleId = null;
	private String activeTrailId = null;
	private RWLowerCaseHashMap<Long> lastGadgetUses = new RWLowerCaseHashMap<>(5, 1f);

	public UserGP(UUID playerUUID) {
		this.playerUUID = playerUUID;
	}

	public UserGP(UUID playerUUID, String activeParticleId, String activeTrailId, RWLowerCaseHashMap<Long> lastGadgetUses) {
		this.playerUUID = playerUUID;
		this.activeParticleId = activeParticleId;
		this.activeParticleId = activeParticleId;
		this.lastGadgetUses = lastGadgetUses;
	}

	public UUID getPlayerUUID() {
		return playerUUID;
	}

	public String getActiveParticleId() {
		return activeParticleId;
	}

	public ElementParticleGP getActiveParticleElement() {
		return ConfigGP.particles.getElement("" + activeParticleId).orNull();
	}

	private transient ParticleRunner activeParticleRunner = null;
	public ParticleRunner getActiveParticleRunner() {
		if (activeParticleRunner == null) {
			final ElementParticleGP elem = getActiveParticleElement();
			final Player player = Bukkit.getPlayer(playerUUID);
			if (elem != null && player != null) {
				activeParticleRunner = elem.getDisplayer().getType().buildRunner(player, elem.getDisplayer());
			}
		}
		return activeParticleRunner;
	}

	public void setActiveParticle(ElementParticleGP particle) {
		this.activeParticleId = particle != null ? particle.getId() : null;
		this.activeParticleRunner = null;
		setToSave();
	}

	public String getActiveTrailId() {
		return activeTrailId;
	}

	public ElementTrail getActiveTrailElement() {
		return ConfigGP.trails.getElement("" + activeTrailId).orNull();
	}

	private transient TrailRunner activeTrailRunner = null;
	public TrailRunner getActiveTrailRunner() {
		if (activeTrailRunner == null) {
			final ElementTrail elem = getActiveTrailElement();
			final Player player = Bukkit.getPlayer(playerUUID);
			if (elem != null && player != null) {
				activeTrailRunner = new TrailRunner(player, elem);
			}
		}
		return activeTrailRunner;
	}

	public void setActiveTrail(ElementTrail trail) {
		this.activeTrailId = trail != null ? trail.getId() : null;
		this.activeTrailRunner = null;
		setToSave();
	}

	public RWLowerCaseHashMap<Long> getLastGadgetUses() {
		return lastGadgetUses;
	}

	@Nullable
	public Long getLastGadgetUse(GadgetType gadgetType) {
		return lastGadgetUses.get(gadgetType.getId());
	}

	public void setLastGadgetUse(GadgetType gadgetType, Long last) {
		if (last == null) {
			lastGadgetUses.remove(gadgetType.getId());
		} else {
			lastGadgetUses.put(gadgetType.getId(), last);
		}
		setToSave();
	}

	// ----- data / cached operations / process / fetch / etc

	public void setToSave() {
		BoardUsersGP.inst().addCachedToSave(playerUUID);
	}

	public static UserGP cachedOrNull(Player key) { return cachedOrNull(key.getUniqueId()); }
	public static UserGP cachedOrNull(UUID key) { return BoardUsersGP.inst().getCachedValue(key); }
	public static void forCached(Player key, Consumer<UserGP> ifCached) { forCached(key.getUniqueId(), ifCached); }
	public static void forCached(UUID key, Consumer<UserGP> ifCached) { UserGP user = cachedOrNull(key); if (user != null) { ifCached.accept(user); } }

}
