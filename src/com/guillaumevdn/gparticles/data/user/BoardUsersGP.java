package com.guillaumevdn.gparticles.data.user;

import java.io.File;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.guillaumevdn.gcore.GCore;
import com.guillaumevdn.gcore.lib.bukkit.BukkitThread;
import com.guillaumevdn.gcore.lib.data.board.keyed.ConnectorKeyed;
import com.guillaumevdn.gcore.lib.data.board.keyed.ConnectorKeyedJson;
import com.guillaumevdn.gcore.lib.data.board.keyed.ConnectorKeyedSQL;
import com.guillaumevdn.gcore.lib.data.board.keyed.KeyedBoardRemote;
import com.guillaumevdn.gcore.lib.data.sql.SQLHandler;
import com.guillaumevdn.gcore.lib.data.sql.SQLiteHandler;
import com.guillaumevdn.gcore.lib.file.FileUtils;
import com.guillaumevdn.gcore.lib.player.PlayerUtils;
import com.guillaumevdn.gparticles.GParticles;

/**
 * @author GuillaumeVDN
 */
public class BoardUsersGP extends KeyedBoardRemote<UUID, UserGP> {

	private static BoardUsersGP instance = null;
	public static BoardUsersGP inst() { return instance; }

	public BoardUsersGP() {
		super(GParticles.inst(), "gparticles_users_v6", UserGP.class, 20 * 60);
		instance = this;
	}

	// ----------------------------------------------------------------------------------------------------
	// data
	// ----------------------------------------------------------------------------------------------------

	@Override
	protected void onInitialized() {
		pullOnline();
	}

	public void pullOnline() {
		Set<UUID> keys = PlayerUtils.getOnlineStream()
				.map(pl -> pl.getUniqueId())
				.collect(Collectors.toSet());
		pullElements(BukkitThread.ASYNC, keys, null);
	}

	@Override
	protected void pulledElement(BukkitThread thread, UUID key, UserGP value) {
		// create if don't exist
		if (value == null) {
			putValue(key, value = new UserGP(key), null, true);
		}
	}

	// ----------------------------------------------------------------------------------------------------
	// json
	// ----------------------------------------------------------------------------------------------------

	@Override
	protected ConnectorKeyed<UUID, UserGP> createConnectorJson() {
		return new ConnectorKeyedJson<UUID, UserGP>(this) {
			@Override
			public File getRoot() {
				return GParticles.inst().getDataFile("data_v6/users/");
			}

			@Override
			public File getFile(UUID key) {
				return GParticles.inst().getDataFile("data_v6/users/" + key + ".json");
			}

			@Override
			public UUID getKey(File file) {
				return UUID.fromString(FileUtils.getSimpleName(file));
			}
		};
	}

	// ----------------------------------------------------------------------------------------------------
	// mysql
	// ----------------------------------------------------------------------------------------------------

	private ConnectorKeyedSQL<UUID, UserGP> createConnectorSQL(SQLHandler handler) {
		return new ConnectorKeyedSQL<UUID, UserGP>(this, handler) {
			@Override
			public String keyName() {
				return "user_uuid";
			}

			@Override
			protected UUID decodeKey(String raw) {
				return UUID.fromString(raw);
			}

			@Override
			protected UserGP decodeValue(String jsonData) {
				return GParticles.inst().getGson().fromJson(jsonData, UserGP.class);
			}

			@Override
			protected String encodeValue(UserGP value) {
				return GParticles.inst().getGson().toJson(value);
			}
		};
	}

	@Override
	protected ConnectorKeyed<UUID, UserGP> createConnectorMySQL() {
		return createConnectorSQL(GCore.inst().getMySQLHandler());
	}

	@Override
	protected ConnectorKeyed<UUID, UserGP> createConnectorSQLite() {
		return createConnectorSQL(new SQLiteHandler(GParticles.inst().getDataFile("data_v6/users.sqlite.db")));
	}

}
