package com.guillaumevdn.gparticles.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.guillaumevdn.gparticles.data.user.BoardUsersGP;

/**
 * @author GuillaumeVDN
 */
public class ConnectionEvents implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PlayerJoinEvent event) {
		final Player player = event.getPlayer();

		BoardUsersGP.inst().fetchValue(player.getUniqueId(), null, null, false, true);
	}

}
