package com.guillaumevdn.gparticles;

import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.permission.PermissionContainer;

/**
 * @author GuillaumeVDN
 */
public class PermissionGP extends PermissionContainer {

	private static PermissionGP instance = null;
	public static PermissionGP inst() { return instance; }

	public PermissionGP() {
		super(GParticles.inst());
		instance = this;
	}

	public final Permission gparticlesAdmin = setAdmin("gparticles.admin");

	public final Permission gparticlesCommandMain = set("gparticles.command.main");
	public final Permission gparticlesCommandEdit = set("gparticles.command.edit");

	public final Permission gparticlesCommandParticle = set("gparticles.command.particle");
	public final Permission gparticlesCommandParticleOthers = set("gparticles.command.particle.others");

	public final Permission gparticlesCommandTrail = set("gparticles.command.trail");
	public final Permission gparticlesCommandTrailOthers = set("gparticles.command.trail.others");

	public final Permission gparticlesCommandGadget = set("gparticles.command.gadget");
	public final Permission gparticlesCommandGadgetOthers = set("gparticles.command.gadget.others");

}
