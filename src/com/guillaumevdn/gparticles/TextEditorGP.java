package com.guillaumevdn.gparticles;

import com.guillaumevdn.gcore.lib.string.TextElement;
import com.guillaumevdn.gcore.lib.string.TextEnumElement;

/**
 * @author GuillaumeVDN
 */
public enum TextEditorGP implements TextEnumElement {

	// ----- particles
	elementParticleName,
	elementParticlePermission,
	elementParticleDisplayer,

	elementParticleDisplayerOffsetOffsetXMin,
	elementParticleDisplayerOffsetOffsetXMax,
	elementParticleDisplayerOffsetOffsetZMin,
	elementParticleDisplayerOffsetOffsetZMax,
	elementParticleDisplayerOffsetOffsetYMin,
	elementParticleDisplayerOffsetOffsetYMax,
	elementParticleDisplayerOffsetDelay,
	elementParticleDisplayerOffsetAmount,
	elementParticleDisplayerOffsetParticles,

	elementParticleDisplayerParticleParticle,
	elementParticleDisplayerParticleColorRed,
	elementParticleDisplayerParticleColorGreen,
	elementParticleDisplayerParticleColorBlue,
	elementParticleDisplayerParticleRedstoneColorScale,
	elementParticleDisplayerParticleNoteColor,

	// ----- trails
	elementTrailName,
	elementTrailPermission,
	elementTrailBlockChangeDelay,
	elementTrailBlockPersistenceTime,
	elementTrailBlockTypes,
	elementTrailVerticalOffset,

	// ----- guis
	elementGUIItemTypeGUIGUI,

	elementGUIItemTypeParticleParticle,
	elementGUIItemTypeParticleIconNoPermission,

	elementGUIItemTypeTrailTrail,
	elementGUIItemTypeTrailIconNoPermission,

	elementGUIItemTypeGadgetGadget,
	elementGUIItemTypeGadgetIconNoPermission,
	elementGUIItemTypeGadgetIconHotbar,

	;

	private TextElement text = new TextElement();

	TextEditorGP() {
	}

	@Override
	public String getId() {
		return name();
	}

	@Override
	public TextElement getText() {
		return text;
	}

}
