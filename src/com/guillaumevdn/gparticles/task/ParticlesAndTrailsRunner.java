package com.guillaumevdn.gparticles.task;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.function.ThrowableRunnable;
import com.guillaumevdn.gcore.lib.player.PlayerUtils;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gparticles.ConfigGP;
import com.guillaumevdn.gparticles.data.user.UserGP;

/**
 * @author GuillaumeVDN
 */
public class ParticlesAndTrailsRunner implements ThrowableRunnable {

	@Override
	public void run() throws Throwable {
		for (Player player : PlayerUtils.getOnline()) {
			final UserGP user = UserGP.cachedOrNull(player);

			if (user != null && (user.getActiveParticleRunner() != null || user.getActiveTrailRunner() != null)) {
				// ensure world is allowed
				if (!ConfigGP.worldRestriction.isAllowed(player.getWorld(), Replacer.of(player))) {
					continue;
				}

				// tick runners
				if (user.getActiveParticleRunner() != null) {
					user.getActiveParticleRunner().run();
				}
				if (user.getActiveTrailRunner() != null) {
					user.getActiveTrailRunner().run();
				}
			}
		}
	}

}
